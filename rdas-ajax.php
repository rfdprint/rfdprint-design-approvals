<?php

//Run external file
include('generate-pdf.php');

/*-------------------------------------------------------------
 Name:      rdas_doc_approval
 Purpose:   Update meta fields via rdas-script.js 
-------------------------------------------------------------*/
function rdas_doc_approval() {
    
       $post_id = $_POST['post_id'];
       $appr_name = $_POST['approver_name']; 
       $date_signed = $_POST['date_signed'];  
       $is_official = $_POST['is_official']; 
       

       if ($is_official == on) {
           $is_official = true;
       } else {
        $is_official = false;
       }

       
       echo  'Design# ' . $post_id . ' Approver Name: '. $appr_name . ' Official?: '. $is_official . 'Signed: ' . $date_signed;
      
       update_post_meta( $post_id , 'doc_approved_chbx', true);
       update_post_meta( $post_id , 'approver_name', $appr_name);
       update_post_meta( $post_id , 'date_doc_approved', $date_signed );
       
       update_post_meta( $post_id , 'signature_official', $is_official);?> 
        
        <?php  die();
        }
//Only logged in users can access this function use this hook        
add_action('wp_ajax_doc_approved', 'rdas_doc_approval');
//Non logged in users can access this function use this hook
add_action('wp_ajax_nopriv_doc_approved', 'rdas_doc_approval');
       
 /*-------------------------------------------------------------
 Name:      doc_approved_notify
 Purpose:   Send approval emails via rdas-script.js 
-------------------------------------------------------------*/  
function doc_approved_notify(){
    check_ajax_referer('my_email_ajax_nonce');
   
    if ( isset($_POST['action']) && $_POST['action'] == "doc_approved_send"){
        $post_id = $_POST['post_id'];

        $approved_email_sent = get_post_meta( $post_id,'doc_approved_email_sent', true ); //check if email already sent
        if ( $approved_email_sent = false ){
            $the_approver_name = $_POST['the_approver_name'];
            $date_signed = $_POST['date_signed'];
            $doc_notes = $_POST['doc_notes'];
            
            //Getting the contents of post using the ID and then placeing it in a string
            //If this query isn't used the function outputs strange values
            //Just using get_the_contents does pass the contant along
            $thepost = new WP_Query('post_type=rdas_design&p='. $post_id);
            if($thepost->have_posts()) {
                while($thepost->have_posts()) {
                    //runs once to get the one queried post
                    $thepost->the_post();
                    $content = wpautop(get_the_content());
                }
            }
            $email_footer_logo = plugins_url('/images//', __FILE__).'rfdprint_logo_small.png';
            $email_footer_img_html =  '<img class="rdas_placeholder" src="'. $email_footer_logo .'" alt="RFDPrint Logo" width="250" height="76" />';
            
            $theplanarray = get_post_meta( $post_id, 'management_plan'); 
            $theplan = $theplanarray[0];

            $thecompanynamearray = get_post_meta($post_id, 'company_name'); 
            $thecompanyname = $thecompanynamearray[0];    

            $thetype =  get_post_type($post_id);

            $toemail = get_post_meta( $post_id, 'doc_client_email', true );
            $design_name = get_the_title($post_id);
            $doc_notes = get_post_meta( $post_id , 'doc_notes', true);

            if ($thetype == 'rdas_agreement'){
            switch ($theplan) {
                case "essential":
                $doctitle = "Essentials Plan Agreement";
                $docsubtitle = "Wordpress Website Management";
                    break;
                case 'premium':
                $doctitle = "Premium Plan Agreement";
                $docsubtitle = "Wordpress Website Management";
                    break;
                case 'business-boost':
                $doctitle = "Business Boost Agreement";
                $docsubtitle = "Wordpress Website Management";
                break;
                case 'social-media':
                $doctitle = "Social Media Agreement";
                $docsubtitle = "Social Content Posting";
                break;    
                case 'license':
                $doctitle = "License Agreement";
                $docsubtitle = "Content Usage";
                break;  
                default:
                    ;
            };             
            agreement_pfd_attatchment($post_id, $the_approver_name, $date_signed, $doc_notes, $theplan, $thecompanyname, $doctitle, '');  

            $pdfname = $date_signed.'_' . $theplan . '_agreement_ref#' . $post_id; 
            $attachments = array(plugin_dir_path(__FILE__).'/docs/agreements//'.$pdfname.'.pdf');
            $html_content = file_get_contents(plugins_url('templates/email/agreement_signed.html', __FILE__));
            $subject = 'An Agreement Has Been Signed Ref# '.$post_id;
            $message = str_replace(['%%APPROVEDBY%%' , '%%DATESIGNED%%', '%%DESIGNNAME%%','%%DOCREFNUMBER%%','%%COMPANYNAME%%', '%%DOCTITLE%%', '%%DOCSUBTITLE%%', '%%FOOTERLOGO%%' ], [$the_approver_name, $date_signed, $design_name, $post_id, $thecompanyname, $doctitle, $docsubtitle, $email_footer_img_html] , $html_content);     
    
        } else {  
                //$html_content = file_get_contents(plugins_url('templates/email/design_approved.html', __FILE__));
                agreement_pfd_attatchment ($post_id, $the_approver_name, $date_signed, $doc_notes, '', $thecompanyname, $design_name, $content);
                $pdfname = $date_signed.'_' . $design_name . '_proof_ref#' . $post_id; 
                $attachments = array(plugin_dir_path(__FILE__).'/docs/designs//'.$pdfname.'.pdf');
                
                $thedesign_thumbnail = get_the_post_thumbnail ($post_id,'medium', true); 
                    if(empty($thedesign_thumbnail)) {
                        $placeholder_img = plugins_url('/images//', __FILE__).'rfdprint_logo_placeholder.png';
                        $thedesign_thumbnail = ' <img class="rdas_placeholder" src="'. $placeholder_img .'" alt="RFDPrint Logo" width="300" height="113" />';
                    }

                $html_content = file_get_contents(plugins_url('templates/email/design_approved.html', __FILE__));
                $subject = 'Your Requested Design Has Been Approved!';
                $message = str_replace(['%%CONTENT%%', '%%APPROVEDBY%%' , '%%DATESIGNED%%', '%%DESIGNNAME%%','%%DOCREFNUMBER%%','%%COMPANYNAME%%','%%DOCTITLE%%','%%FOOTERLOGO%%'], [$thedesign_thumbnail, $the_approver_name, $date_signed, $design_name, $post_id, $thecompanyname, $doctitle, $email_footer_img_html] , $html_content);
        }          

        $headers[] = 'Cc: RFDPrint <sales@rfdprint.com>';
        $headers[] = 'Content-Type: text/html; charset=UTF-8'; 

        //send email  wp_mail( $to, $subject, $message, $headers, $attachments ); ex: wp_mail($_POST['toemail']
            wp_mail($toemail , $subject , $message , $headers, $attachments);
            $sent_success = true;
            update_post_meta( $post_id , 'doc_approved_email_sent', $sent_success);
            echo 'email sent';
            die();
        } else { 
        echo 'email alreay sent';
        die();
        }
    }    
    echo 'error';
    die();
}
//Only logged in users can access this function use this hook           
add_action('wp_ajax_doc_approved_send', 'doc_approved_notify');
//Non logged in users can access this function use this hook
add_action('wp_ajax_nopriv_doc_approved_send', 'doc_approved_notify');   

/*-------------------------------------------------------------
 Name:      doc_ready_notify
 Purpose:   Send doc ready emails via rdas-script.js 
-------------------------------------------------------------*/  
function doc_ready_notify(){
    check_ajax_referer('my_email_ajax_nonce');
    if ( isset($_POST['action']) && $_POST['action'] == "doc_ready_send" ){
        
        $post_id = $_POST['post_id']; 
        $date_ready = $_POST['date_ready'];          
        //Getting the contents of post usin the ID and then placeing it in a string
        //If this query isn't used the function outputs strange values
        $thepost = new WP_Query('post_type=rdas_design&p='. $post_id);
        if($thepost->have_posts()) {
            while($thepost->have_posts()) {
                //runs once to get the one queried post
                $thepost->the_post();
                $content = wpautop(get_the_content());
            }
        }
        $email_footer_logo = plugins_url('/images//', __FILE__).'rfdprint_logo_small.png';
        $email_footer_img_html =  '<img class="rdas_placeholder" src="'. $email_footer_logo .'" alt="RFDPrint Logo" width="250" height="76" />';
        
        $theplanarray = get_post_meta( $post_id, 'management_plan'); 
        $theplan = $theplanarray[0];
        $thecompanynamearray = get_post_meta($post_id, 'company_name'); 
        $thecompanyname = $thecompanynamearray[0];    

        $thetype =  get_post_type($post_id);
        if ($thetype == 'rdas_agreement'){
            switch ($theplan) {
                case "essential":
                $doctitle = "Essentials Plan Agreement";
                $docsubtitle = "Wordpress Website Management";
                    break;
                case 'premium':
                $doctitle = "Premium Plan Agreement";
                $docsubtitle = "Wordpress Website Management";
                    break;
                case 'business-boost':
                $doctitle = "Business Boost Agreement";
                $docsubtitle = "Wordpress Website Management";
                break;
                case 'social-media':
                $doctitle = "Social Media Agreement";
                $docsubtitle = "Social Content Posting";
                break;      
                case 'license':
                $doctitle = "License Agreement";
                $docsubtitle = "Content Usage";
                break;  
                default:
                    ;
            }; 
        }
        $thetype =  get_post_type($post_id); 
        $toemail = get_post_meta( $post_id, 'doc_client_email', true );
        $design_name = get_the_title($post_id);
        $design_url = get_post_permalink($post_id);
        $design_title = get_the_title($post_id);           
        
        if ($thetype == 'rdas_agreement'){
            $html_content = file_get_contents(plugins_url('templates/email/agreement_ready.html', __FILE__));
            $subject = $doctitle. ' #'.$post_id.' Is Ready For Review';
            $message = str_replace(['%%DESIGNNAME%%', '%%DESIGNURL%%', '%%DOCTITLE%%', '%%DOCSUBTITLE%%','%%DOCREFNUMBER%%', '%%FOOTERLOGO%%' ], [$design_name, $design_url, $doctitle, $docsubtitle, $post_id, $email_footer_img_html ] , $html_content);
        } else {
                $html_content = file_get_contents(plugins_url('templates/email/design_ready.html', __FILE__));
                $subject = 'Design Review Ready: ' . $design_title;
        
                $thedesign_thumbnail = get_the_post_thumbnail ($post_id,'medium', true); 
                if(empty($thedesign_thumbnail)) {
                    $placeholder_img = plugins_url('/images//', __FILE__).'rfdprint_logo_placeholder.png';
                    $thedesign_thumbnail = ' <img class="rdas_placeholder" src="'. $placeholder_img .'" alt="RFDPrint Logo" width="300" height="113" />';
                }
                $message = str_replace(['%%CONTENT%%', '%%DESIGNNAME%%', '%%DESIGNURL%%','%%DOCREFNUMBER%%', '%%FOOTERLOGO%%' ,'%%COMPANYNAME%%' ], [$thedesign_thumbnail, $design_name, $design_url, $post_id, $email_footer_img_html, $thecompanyname] , $html_content);
            }
                        
        $headers[] = 'Cc: RFDPrint <sales@rfdprint.com>';
        $headers[] = 'Content-Type: text/html; charset=UTF-8';
            //send email  wp_mail( $to, $subject, $message, $headers, $attachments ); ex: wp_mail($_POST['toemail']    
        wp_mail($toemail , $subject , $message , $headers);
        echo 'email sent';

            //Update Log
            $currentdoclog = get_post_meta( $post_id, 'rdas_document_log', true );
            $updatedoclog = 'Ready Email Sent: ' . date("n-d-Y - g:i A") . "\n" . $currentdoclog;
            update_post_meta( $post_id , 'rdas_document_log', $updatedoclog );
        
        die();
    }
    echo 'error';
    die();
}
//Only logged in users can access this function use this hook   
add_action('wp_ajax_doc_ready_send', 'doc_ready_notify');
//Non logged in users can access this function use this hook
add_action('wp_ajax_nopriv_doc_ready_send', 'doc_ready_notify');
?>