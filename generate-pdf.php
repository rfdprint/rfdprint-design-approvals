<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
/*-------------------------------------------------------------
 Name:      agreement_pfd_attatchment
 Purpose:   Creates a pdf of HTML content
-------------------------------------------------------------*/
function agreement_pfd_attatchment ($post_id, $the_approver_name, $date_signed, $doc_notes, $theplan, $thecompanyname, $doctitle, $thedesign){

// Include the main TCPDF library (search for installation path).
require_once( plugin_dir_path( __FILE__ ) .'TCPDF/tcpdf.php');

//The Plan
$thetype = get_post_type($post_id);
$notestitle = 'ADDITIONAL SERVICES';
$signer_label = 'Signed';
if ( $thetype == 'rdas_agreement') {
    switch ($theplan) {
        case "essential":
        $header_title = $doctitle. ' - Wordpress Website Management Ref# '. $post_id;
        $thecontent = file_get_contents(plugin_dir_path(__FILE__) . '/templates/wwm-essentials.html');
        $approvedby = '<div style="text-align:center; color: #000000; font-weight: 700;font-size: 20px; background-color: #f2f2f2; padding-bottom: 10px;">Signed By: %%APPROVEDBY%%</div>';  
        break;
        case 'premium':
        $header_title = $doctitle. ' - Wordpress Website Management Ref# '. $post_id;
        $thecontent = file_get_contents(plugin_dir_path(__FILE__) . '/templates/wwm-premium.html');
        $approvedby = '<div style="text-align:center; color: #000000; font-weight: 700;font-size: 20px; background-color: #f2f2f2; padding-bottom: 10px;">Signed By: %%APPROVEDBY%%</div>'; 
            break;
        case 'business-boost':
        $header_title = $doctitle. ' - Wordpress Website Management Ref# '. $post_id;
        $thecontent = file_get_contents(plugin_dir_path(__FILE__) . '/templates/wwm-business-boost.html');
        $approvedby = '<div style="text-align:center; color: #000000; font-weight: 700;font-size: 20px; background-color: #f2f2f2; padding-bottom: 10px;">Signed By: %%APPROVEDBY%%</div>'; 
        break;
        case 'social-media':
        $header_title = $doctitle. ' - Content Posting Ref# '. $post_id;
        $thecontent = file_get_contents(plugin_dir_path(__FILE__) . '/templates/ssm-standard.html');
        $approvedby = '<div style="text-align:center; color: #000000; font-weight: 700;font-size: 20px; background-color: #f2f2f2; padding-bottom: 10px;">Signed By: %%APPROVEDBY%%</div>'; 
        break;     
        
        case 'license':
        $header_title = $doctitle. ' - License Agreement Ref# '. $post_id;
        $thecontent = file_get_contents(plugin_dir_path(__FILE__) . '/templates/la-license.html');
        $approvedby = '<div style="text-align:center; color: #000000; font-weight: 700;font-size: 20px; background-color: #f2f2f2; padding-bottom: 10px;">Signed By: %%APPROVEDBY%%</div>'; 
        break;    
        default:
            ;
    };
    $theterms = '';
} else {

    $header_title = $doctitle. ' - Proof Ref# '. $post_id;
    $thecontent = file_get_contents(plugin_dir_path(__FILE__) . '/templates/pdf_designs.html');
    $theterms = '<p id="theterms" style="color:black; font-size: 13px;"> TERMS AND CONDITIONS Designs cannot be changed after approval. You, the business owner or representative, accept any and all responsibility for elements, offers, and content of the approved design. You the business owner or representative, accept full responsibility and certify that you own or have license rights to any materials or content submitted to RFDPrint by you or your representative that are included on this design and release RFDPrint from any litigation, disputes, loss of business or finances as result of violations of copyright, trademark or privacy laws. RFDPrint has the right to use this approved design on printed or online content as samples, templates and/or promotions of RFDPrint.</p>/n';
    $notestitle = '<hr>ADDITIONAL INFORMATION';
    $approvedby ='';
    $signer_label = 'Approved';
}
$thedate = 'Date: ' . date("F Y g:i A") . ' (Central Time)';
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('RFDPrint');
$pdf->SetTitle('Approval Document');
$pdf->SetSubject('Document Approval');
$pdf->SetKeywords('PDF, agreement, RFDPrint');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
//$pdf->setFooterData(array(0,64,0), array(0,64,128));
//$header_title = $doctitle.' Ref# '. $post_id . ' - Wordpress Website Management' . 'Date: ' .date("m-d-Y h:i:s") . '(Central Time)';


$pdf->SetHeaderData(PDF_HEADER_LOGO, 30, $thedate , $header_title, array(0, 51, 0), array(0, 51, 0));
$pdf->setFooterData(array(0,51,0), array(0, 51, 0));


// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
// set default monospaced font

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 14, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
$doc = '<style>
            h1 {
                color: #003300;
                font-family: times;
                font-size: 24pt;
                line-height: -25px;
            }
            .main-header-title, .design-content { 
                color: #003300;
                line-height: -50px;
            }
          
            .pdf-design-approved-header {
                line-height: -10px
            }
            .pdf-remove-space { 
                line-height: -20px;
            }
            .pdf-remove-space-p {
                float: none;
                line-height: 20px;
            }
            .reduce-line-height{
                line-height: -20px;
            }
            /*.add-line-height{
                line-height: 50px;
            }*/
        </style>'
        . $thecontent . ' ' . $theterms . 
        '<hr><h4 class="reduce-line-height" style="text-align:left; color: #000000;">'. $notestitle .'</h4>
        <p style="font-size: 15px;">%%DOCNOTES%%</p>'
        .$approvedby.    
        '<div style="text-align:center; color: #000000; font-weight: 700;font-size: 20px; background-color: #f2f2f2; padding-bottom: 10px;">Date '.$signer_label.': %%DATESIGNED%%</div>';

$html = str_replace(['%%APPROVEDBY%%' , '%%DATESIGNED%%', '%%DOCNOTES%%', '%%DOCREFNUMBER%%','%%COMPANYNAME%%', '%%DOCTITLE%%', '%%DESIGNPROOF%%','%%SIGNATURES%%', '%%DESIGNTERMS%%'], [$the_approver_name, $date_signed,  $doc_notes, $post_id, $thecompanyname, $doctitle, $thedesign] , $doc);        
// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.

// Clean any content of the output buffer

ob_end_clean();
//$thefile = $pdf->Output('example_001.pdf', 'S');
if ( $thetype == 'rdas_agreement') {
$pdfname =$date_signed.'_' . $theplan . '_agreement_ref#' . $post_id;
$PdfName = plugin_dir_path(__FILE__).'/docs/agreements//'.$pdfname.'.pdf';
} else {
    if ( $thetype == 'rdas_design') {
        $pdfname =$date_signed.'_' . $doctitle. '_proof_ref#' . $post_id;
        $PdfName = plugin_dir_path(__FILE__).'/docs/designs//'.$pdfname.'.pdf';
}
}

//$PdfName = plugins_url('/docs//', __FILE__) .$pdfname.'.pdf';
echo $pdf->Output($PdfName, 'F');
echo $pdf->Output($pdfname.'.pdf', 'D');


//============================================================+
// END OF FILE
//============================================================+
//return $thefile;
}
