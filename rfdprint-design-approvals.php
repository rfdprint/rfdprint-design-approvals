<?php 
/* Description:       This plugin creates a design approval system
 * Plugin Name:       RFDPrint Design Approvals
 * Plugin URI:        https://rfdprint.com/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            RFDPrint
 * Author URI:        https://rfdprint.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       RFDPrint Design Approvals
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'EDD_VERSION', '1.0.3' ); //Defines the script version number
 define( 'RFDPrint Design Approvals', EDD_VERSION );

/*-------------------------------------------------------------
 Name:      wpdocs_codex_design_project_init
 Purpose:   Register a custom posttype: projects
-------------------------------------------------------------*/
function wpdocs_codex_design_project_init() {
    $labels = array(
        'name'                  => _x( 'Projects', 'Post type general name', 'textdomain' ),
        'singular_name'         => _x( 'Project', 'Post type singular name', 'textdomain' ),
        'name_admin_bar'        => _x( 'Projects', 'Add New on Toolbar', 'textdomain' ),
        'menu_name'             => _x( 'Projects', 'Admin Menu text', 'textdomain' ),
        'name_admin_bar'        => _x( 'Projects', 'Add New on Toolbar', 'textdomain' ),
        'add_new'               => __( 'Add New', 'textdomain' ),
        'add_new_item'          => __( 'Add New Project', 'textdomain' ),
        'new_item'              => __( 'New Project', 'textdomain' ),
        'edit_item'             => __( 'Edit Project', 'textdomain' ),
        'view_item'             => __( 'View Project', 'textdomain' ),
        'all_items'             => __( 'All Projects', 'textdomain' ),
        'search_items'          => __( 'Search Projects', 'textdomain' ),
        'parent_item_colon'     => __( 'Parent Projects:', 'textdomain' ),
        'not_found'             => __( 'No Projects found.', 'textdomain' ),
        'not_found_in_trash'    => __( 'No Projects found in Trash.', 'textdomain' ),
        'featured_image'        => _x( 'Project Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'archives'              => _x( 'Project archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
        'insert_into_item'      => _x( 'Insert into Project', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this Project', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
        'filter_items_list'     => _x( 'Filter Projectlist', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
        'items_list_navigation' => _x( 'Project list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
        'items_list'            => _x( 'Projects list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
    );
 
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'projects' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'taxonomies'         => array( 'category' ),
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-pressthis',
        'supports'           => array('thumbnail', 'title', 'page-attributes', 'editor', 'comments'),
    );
 
    register_post_type( 'rdas_projects', $args );
}
 
add_action( 'init', 'wpdocs_codex_design_project_init' );

/*-------------------------------------------------------------
 Name:      wpdocs_codex_design_init
 Purpose:   Register a custom posttype: designs
-------------------------------------------------------------*/
function wpdocs_codex_design_init() {
    
    $labels = array(
        'name'                  => _x( 'Design Projects', 'Post type general name', 'textdomain' ),
        'singular_name'         => _x( 'Design', 'Post type singular name', 'textdomain' ),
        'name_admin_bar'        => _x( 'Designs', 'Add New on Toolbar', 'textdomain' ),
        'menu_name'             => _x( 'Design', 'Admin Menu text', 'textdomain' ),
        'name_admin_bar'        => _x( 'Design', 'Add New on Toolbar', 'textdomain' ),
        'add_new'               => __( 'Add New', 'textdomain' ),
        'add_new_item'          => __( 'Add New Design', 'textdomain' ),
        'new_item'              => __( 'New Design', 'textdomain' ),
        'edit_item'             => __( 'Edit Design', 'textdomain' ),
        'view_item'             => __( 'View Design', 'textdomain' ),
        'all_items'             => __( 'Designs', 'textdomain' ),
        'search_items'          => __( 'Search Designs', 'textdomain' ),
        'parent_item_colon'     => __( 'Parent Designss:', 'textdomain' ),
        'not_found'             => __( 'No Designs found.', 'textdomain' ),
        'not_found_in_trash'    => __( 'No Designs found in Trash.', 'textdomain' ),
        'featured_image'        => _x( 'Design Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'archives'              => _x( 'Design archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
        'insert_into_item'      => _x( 'Insert into Design', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this Design', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
        'filter_items_list'     => _x( 'Filter Design list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
        'items_list_navigation' => _x( 'Designs list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
        'items_list'            => _x( 'Designs list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
    );
    
 
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => 'edit.php?post_type=rdas_projects',
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'design'),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => true,
        'taxonomies'         => array( 'category' ),
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-pressthis',
        'supports'           => array('editor', 'thumbnail', 'excerpt', 'title', 'page-attributes','comments' ),
        'parent'             => 'Projects',
    );
    register_post_type( 'rdas_design', $args );
}
add_action( 'init', 'wpdocs_codex_design_init' );

/*-------------------------------------------------------------
 Name:      (TEMP) wpdocs_codex_temp_design_init
 Purpose:   This is a temporary post typr that will allow a transfer to the permenant post type 
            using "Post Type Swicther" onces all the old "designs" post types are transfered this
            type can be removed.
-------------------------------------------------------------*/
function wpdocs_codex_temp_design_init() {
    
    $labels = array(
        'name'                  => _x( 'Design Projects', 'Post type general name', 'textdomain' ),
        'singular_name'         => _x( 'Design (original)', 'Post type singular name', 'textdomain' ),
        'name_admin_bar'        => _x( 'Designs (original)', 'Add New on Toolbar', 'textdomain' ),
        'menu_name'             => _x( 'Design (original)', 'Admin Menu text', 'textdomain' ),
        'name_admin_bar'        => _x( 'Design (original)', 'Add New on Toolbar', 'textdomain' ),
        'add_new'               => __( 'Add New', 'textdomain' ),
        'add_new_item'          => __( 'Add New Design (original)', 'textdomain' ),
        'new_item'              => __( 'New Design (original)', 'textdomain' ),
        'edit_item'             => __( 'Edit Design', 'textdomain' ),
        'view_item'             => __( 'View Design (original)', 'textdomain' ),
        'all_items'             => __( 'Designs (original)', 'textdomain' ),
        'search_items'          => __( 'Search Designs', 'textdomain' ),
        'parent_item_colon'     => __( 'Parent Designss:', 'textdomain' ),
        'not_found'             => __( 'No Designs found.', 'textdomain' ),
        'not_found_in_trash'    => __( 'No Designs found in Trash.', 'textdomain' ),
        'featured_image'        => _x( 'Design Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'archives'              => _x( 'Design archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
        'insert_into_item'      => _x( 'Insert into Design', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this Design', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
        'filter_items_list'     => _x( 'Filter Design list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
        'items_list_navigation' => _x( 'Designs list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
        'items_list'            => _x( 'Designs list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
    );
    
 
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => 'edit.php?post_type=rdas_projects',
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'designs'),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => true,
        'taxonomies'         => array( 'category' ),
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-pressthis',
        'supports'           => array('editor', 'thumbnail', 'excerpt', 'title', 'page-attributes','comments' ),
        'parent'             => 'Projects',
    );
    register_post_type( 'designs', $args );
}
add_action( 'init', 'wpdocs_codex_temp_design_init' );

/*array( 
    'slug' => 'products', // use this slug instead of post type name
    'with_front' => FALSE // if you have a permalink base such as /blog/ then setting this to false ensures your custom post type permalink structure will be /products/ instead of /blog/products/
),*/

/*-----------------------------------------------------
 Name:      wpdocs_codex_agreem---ent_init
 Purpose:   Register a custom posttype: agreements
-------------------------------------------------------------*/
function wpdocs_codex_agreement_init() {
    $labels = array(
        'name'                  => _x( 'Agreements', 'Post type general name', 'textdomain' ),
        'singular_name'         => _x( 'Agreement', 'Post type singular name', 'textdomain' ),
        'name_admin_bar'        => _x( 'Agreements', 'Add New on Toolbar', 'textdomain' ),
        'menu_name'             => _x( 'Agreement', 'Admin Menu text', 'textdomain' ),
        'name_admin_bar'        => _x( 'Agreement', 'Add New on Toolbar', 'textdomain' ),
        'add_new'               => __( 'Add New', 'textdomain' ),
        'add_new_item'          => __( 'Add New Agreement', 'textdomain' ),
        'new_item'              => __( 'New Agreement', 'textdomain' ),
        'edit_item'             => __( 'Edit Agreement', 'textdomain' ),
        'view_item'             => __( 'View Agreement', 'textdomain' ),
        'all_items'             => __( 'Agreements', 'textdomain' ),
        'search_items'          => __( 'Search Agreements', 'textdomain' ),
        'parent_item_colon'     => __( 'Parent Agreements:', 'textdomain' ),
        'not_found'             => __( 'No Agreements found.', 'textdomain' ),
        'not_found_in_trash'    => __( 'No Agreements found in Trash.', 'textdomain' ),
        'featured_image'        => _x( 'Agreement Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'archives'              => _x( 'Agreement archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
        'insert_into_item'      => _x( 'Insert into Agreement', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this Agreement', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
        'filter_items_list'     => _x( 'Filter Agreement list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
        'items_list_navigation' => _x( 'Agreements list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
        'items_list'            => _x( 'Agreements list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
    );
 
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => 'edit.php?post_type=rdas_projects',
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'agreement' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => true,
        'taxonomies'         => array( 'category' ),
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-pressthis',
        'supports'           => array('thumbnail','title', 'page-attributes','comments' ),
        'parent'             => 'Projects',
    );
 
    register_post_type( 'rdas_agreement', $args );
}
 
add_action( 'init', 'wpdocs_codex_agreement_init' );


/*-----------------------------------------------------
(TEMP) wpdocs_codex_temp_agreement_init
 Purpose:   This is a temporary post typr that will allow a transfer to the permenant post type 
            using "Post Type Swicther" onces all the old "designs" post types are transfered this
            type can be removed.
-------------------------------------------------------------*/
function wpdocs_codex_temp_agreement_init() {
    $labels = array(
        'name'                  => _x( 'Agreements (original)', 'Post type general name', 'textdomain' ),
        'singular_name'         => _x( 'Agreement (original)', 'Post type singular name', 'textdomain' ),
        'name_admin_bar'        => _x( 'Agreements (original)', 'Add New on Toolbar', 'textdomain' ),
        'menu_name'             => _x( 'Agreement (original)', 'Admin Menu text', 'textdomain' ),
        'name_admin_bar'        => _x( 'Agreement (original)', 'Add New on Toolbar', 'textdomain' ),
        'add_new'               => __( 'Add New (original)', 'textdomain' ),
        'add_new_item'          => __( 'Add New Agreement', 'textdomain' ),
        'new_item'              => __( 'New Agreement', 'textdomain' ),
        'edit_item'             => __( 'Edit Agreement', 'textdomain' ),
        'view_item'             => __( 'View Agreement', 'textdomain' ),
        'all_items'             => __( 'Agreements (original)', 'textdomain' ),
        'search_items'          => __( 'Search Agreements', 'textdomain' ),
        'parent_item_colon'     => __( 'Parent Agreements:', 'textdomain' ),
        'not_found'             => __( 'No Agreements found.', 'textdomain' ),
        'not_found_in_trash'    => __( 'No Agreements found in Trash.', 'textdomain' ),
        'featured_image'        => _x( 'Agreement Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'archives'              => _x( 'Agreement archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
        'insert_into_item'      => _x( 'Insert into Agreement', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this Agreement', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
        'filter_items_list'     => _x( 'Filter Agreement list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
        'items_list_navigation' => _x( 'Agreements list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
        'items_list'            => _x( 'Agreements list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
    );
 
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => 'edit.php?post_type=rdas_projects',
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'agreement' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => true,
        'taxonomies'         => array( 'category' ),
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-pressthis',
        'supports'           => array('thumbnail','title', 'page-attributes','comments' ),
        'parent'             => 'Projects',
    );
 
    register_post_type( 'agreements', $args );
}
 
add_action( 'init', 'wpdocs_codex_temp_agreement_init' );


/*-------------------------------------------------------------
 Name:      rdas_projects_add_page_template
 Purpose:   Finds the template 
            in the plugin folder and regesters it.
Template:   Projects: single-projects.php          
-------------------------------------------------------------*/
/*--Assigns a name to the plugin registered template 
for the backend posttype page*/
function rdas_projects_add_page_template ($templates) {
    $templates['single-projects.php'] = 'Projects Template';
    return $templates;
    }
add_filter ('theme_rdas_projects_templates', 'rdas_projects_add_page_template');

/* Redirects the wp from the theme folder to the plugin folder */
function rdas_project_redirect_page_template ($template) {
    $post = get_post(); $page_template = get_post_meta( $post->ID, '_wp_post_template', true ); 
    
    if ('single-projects.php' == basename ($page_template )) 
        $template = plugin_dir_path( __FILE__ ) . '/templates/single-projects.php'; 
    return $template;
    }
add_filter ('rdas_projects_template', 'rdas_project_redirect_page_template ');

/* Auto assigns the custom template based on post type */
function rdas_projects_custom_template($single) {
    global $wp_query, $post;

    /* Checks for single template by post type */
    if ( $post->post_type == 'rdas_projects' ) {
        if ( file_exists( plugin_dir_path( __FILE__ ) . '/templates/single-projects.php' ) ) {
            return plugin_dir_path( __FILE__ ) . 'templates/single-projects.php';
        }
    }
    return $single;

}
/* Filter the single_template with our custom function */
add_filter('single_template', 'rdas_projects_custom_template');


/*-------------------------------------------------------------
 Name:      rdas_designs_add_page_template
 Purpose:   Finds the template 
            in the plugin folder and regesters it.
Template:   Designs Template: single-designs.php          
-------------------------------------------------------------*/
/*--Assigns a name to the plugin registered template 
for the backend posttype page*/
function rdas_designs_add_page_template ($templates) {
    $templates['single-designs.php'] = 'Designs Template';
    return $templates;
    }
add_filter ('theme_designs_templates', 'rdas_designs_add_page_template');

/* Redirects the wp from the theme folder to the plugin folder */
function rdas_designs_redirect_page_template ($template) {
    $post = get_post(); $page_template = get_post_meta( $post->ID, '_wp_post_template', true ); 
    
    if ('single-designs.php' == basename ($page_template )) 
        $template = plugin_dir_path( __FILE__ ) . '/templates/single-designs.php'; 
    return $template;
    }
add_filter ('designs_template', 'rdas_designs_redirect_page_template ');

/* Auto assigns the custom template based on post type */
function rdas_designs_custom_template($single) {
    global $wp_query, $post;

    /* Checks for single template by post type */
    if ( $post->post_type == 'rdas_design' ) {
        if ( file_exists( plugin_dir_path( __FILE__ ) . '/templates/single-designs.php' ) ) {
            return plugin_dir_path( __FILE__ ) . 'templates/single-designs.php';
        }
    }
    return $single;

}
/* Filter the single_template with our custom function*/
add_filter('single_template', 'rdas_designs_custom_template');


/*-------------------------------------------------------------
 Name:      agreements_add_page_template
 Purpose:   Finds the template 
            in the plugin folder and regesters it.
Template:   Agreements Template: single-agreements.php          
-------------------------------------------------------------*/
/*--Assigns a name to the plugin registered template 
for the backend posttype page*/
function agreements_add_page_template ($templates) {
    $templates['single-agreements.php'] = 'Agreements Template';
    return $templates;
    }
add_filter ('theme_agreements_templates', 'agreements_add_page_template');

/* Redirects the wp from the theme folder to the plugin folder */
function agreements_redirect_page_template ($template) {
    $post = get_post(); $page_template = get_post_meta( $post->ID, '_wp_post_template', true ); 
    
    if ('single-agreements.php' == basename ($page_template )) 
        $template = plugin_dir_path( __FILE__ ) . '/templates/single-agreements.php'; 
    return $template;
    }
add_filter ('agreements_template', 'agreements_redirect_page_template ');

function agreements_custom_template($single) {
    global $wp_query, $post;

    /* Auto assigns the custom template based on post type */
    if ( $post->post_type == 'rdas_agreement' ) {
        if ( file_exists( plugin_dir_path( __FILE__ ) . '/templates/single-agreements.php' ) ) {
            return plugin_dir_path( __FILE__ ) . 'templates/single-agreements.php';
        }
    }
    return $single;

}
add_filter('single_template', 'agreements_custom_template');

/*-------------------------------------------------------------
 Name:      rdas_comment_template
 Purpose:   Redirects wp to custom comments template based on
            posttype
Comments:   comments-requests.php         
-------------------------------------------------------------*/
function rdas_comment_template( $comment_template ) {
     global $post;
     if ( !( is_singular() && ( have_comments() || 'open' == $post->comment_status ) ) ) {
        return;
     }
     if($post->post_type == 'rdas_design' || $post->post_type == 'rdas_agreement'  ){ // assuming there is a post type called business
        return plugin_dir_path( __FILE__ ) . 'templates/comments-template.php';
     }
}
add_filter( "comments_template", "rdas_comment_template" );



/*-------------------------------------------------------------
 Name:      get_designs_archive_template
 Purpose:   Redirects wp to custom archive template based on
            posttype
Comments:   archive-designs.php        
-------------------------------------------------------------*/
function get_designs_archive_template($template){
    if(is_post_type_archive('rdas_design')){
      $theme_files = array('archive-designs.php');
      $exists_in_theme = locate_template($theme_files, false);
      if($exists_in_theme == ''){
        return plugin_dir_path(__FILE__) . '/templates/archive-designs.php';
      }
    }
    return $template;
  }
  add_filter('archive_template', 'get_designs_archive_template');


/*-------------------------------------------------------------
 Name:      rdas_my_assets
 Purpose:   Registers scripts and styles and enqueues them based
            on posttype.  This is to make sure that the needed
            scripts and styles are only loaded on the pages they
            are needed. 
-------------------------------------------------------------*/


function rdas_my_assets() {
    wp_register_style ( 'rdas-stylesheet', plugins_url('/css/rdas-stylesheet.css', __FILE__), EDD_VERSION );
    wp_register_script( 'sweetalerts', plugins_url('/js/sweetalert.min.js', __FILE__) );
    wp_register_script( 'rdas-script', plugins_url('/js/rdas-script.js', __FILE__) , array( 'jquery','sweetalerts' ), EDD_VERSION);
    wp_localize_script( 'rdas-script', 'ajax_object',
                array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
    wp_register_script ('googlerecaptcha', "https://www.google.com/recaptcha/api.js");
  
    if ( 'rdas_design' === get_post_type() ||  'rdas_agreement' === get_post_type()){
        wp_enqueue_script('rdas-script' );
        wp_enqueue_script('sweetalerts');
        wp_enqueue_style( 'rdas-stylesheet');
    }
}
add_action( 'wp_enqueue_scripts', 'rdas_my_assets' );

include('rdas-ajax.php'); 

//Runs an external php file
function rdas_get_files(){
    if ( 'rdas_design' === get_post_type() ||  'rdas_agreement' === get_post_type()){
    require_once('rdas-comments.php');
    }
}
add_action( 'wp_enqueue_scripts', 'rdas_get_files' );

function editing_comment_mail_subject($subject, $comment_id ){
    global $posts;
    $comment = get_comment( $comment_id );
    $post    = get_post( $comment->comment_post_ID );
 
    $subject = sprintf( __( ' Design Change Request For: "%s" ' ), $post->post_title ) ;
    return $subject; 
 }
 add_action( 'comment_notification_subject', 'editing_comment_mail_subject', 10, 2); 

//Sets the defaul timezone of the server to Central
date_default_timezone_set("America/Chicago");

/**
 * Auto generate numeric slugs for a given custom post type (via post.php screen)
 * Requires PHP 5.3+
 * @see http://wordpress.stackexchange.com/a/169907/26350
 */
add_action( 'load-post.php', function(){
    add_filter( 'wp_insert_post_data', function( $data, $postarr ) {
        if(
            'post' === $data['rdas_design']   // <-- Edit the cpt to your needs!
            || 'post' === $data['rdas_agreement'] 
            && empty( $data['post_name'] )
            && isset( $postarr['post_ID'] )
            && $postarr['post_ID'] * 1 > 0
        )
        {
            //----------------------------------
            // Method with post ID reference (not random):
            //
            $data['post_name'] = sprintf( '%u', crc32( $postarr['post_ID'] ) );

            //----------------------------------
            // Method without post ID reference (random):
            //                 
            // $data['post_name'] = rand( 10000000, 99999999 );        
        }
        return $data;
    }, 10, 2 );
});

/*add_filter( 'default_content', 'my_editor_content', 10, 2 );
 
function my_editor_content( $content, $post ) {
 
    switch( $post->post_type ) {
        case 'designs':
            $content = '<div class="watermark-notice">
            <p>The RFDPrint watermark will be removed from the final product.</p>
            </div>';
        break;
  
    }
 
    return $content;
}*/

/*-------------------------------------------------------------
 Purpose:   Sets Referer Headers
-------------------------------------------------------------*/
//add_action('send_headers', function(){ 
// Referrer Policy
//header("Referrer-Policy: strict-origin");
//header("Referrer-Policy: no-referrer-when-downgrade");
//}, 1);

/**
 * Auto generate numeric slugs for a given custom post type (via post.php screen)
 * Requires PHP 5.3+
 * @see http://wordpress.stackexchange.com/a/169907/26350
 */

function append_slug($data) {
    global $post_ID;
    
    if (!empty($data['post_name']) && $data['post_status'] == "publish" && ($data['post_type'] == "rdas_design"
        || $data['post_type'] == "rdas_agreement")) {
    
            if( !is_numeric(substr($data['post_name'], -4)) ) {
                $random = rand(1111111,9999999);
                $data['post_name'] = sanitize_title($data['post_title'], $post_ID);
                $data['post_name'] .= '-' . $random;
            }
    
    }
     return $data; }
      add_filter('wp_insert_post_data', 'append_slug', 10);

?>