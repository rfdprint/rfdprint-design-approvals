
jQuery(function($){

    jQuery(document).ready(function($) {
        $('#thedesignterms').hide();

        $('#showterms').click(function(){
            if($('#thedesignterms').is(':visible')){
            $('#thedesignterms').hide();
            $("#showterms").text("Show Terms & Conditions");
        } else { 
            $('#thedesignterms').show();
            $("#showterms").text("Hide Terms & Conditions");
        }
        });     
        
       
       //Disable cut copy paste
        $('body').bind('cut copy paste', function (e) {
            e.preventDefault();
        });
        
        //Disable mouse right click
        $("body").on("contextmenu",function(e){
            return false;
        });
    

    function GetTodayDate() {
    var tdate = new Date();
    var dd = tdate.getDate(); //yields day
    var MM = tdate.getMonth(); //yields month
    var yyyy = tdate.getFullYear(); //yields year
    var currentDate= ( MM+1) + "-" + dd + "-" + yyyy;

    return currentDate;
    }

    
    $('button#approval-btn').click(function(){

        
        $the_doc_id = $('button#approval-btn').val();
        $the_approver_name = $('input#approver-name').val();
        $is_official = $('#approver-chbx').val();
        $termschbx = $('#doc-terms-chbx').prop('checked');
        $date_signed = GetTodayDate();
        $theposttype = $('#theposttype').val();

        if ($the_approver_name == '' || $termschbx == false || ( $('#approver-chbx').prop('checked')==false) ){
            swal({
                title: "Missing Required Fields!",
                text: "All fields on this form are required fields",
                icon: "error",
              });        } else {
                    var data = { 
                        action: 'doc_approved',
                        post_id: $the_doc_id,
                        approver_name: $the_approver_name,
                        date_signed: $date_signed,
                        is_official: $is_official,
                        
                        success: function(){
                           if ($theposttype == 'agreements'){
                            swal("This Agreement Has Been Signed",'', "success");
                            $('button#approval-btn').text("Agreement Signed");
                            $('button#approval-btn').css({'background-color':'blue'});
                            $('#show-approvals-btn').text("Agreement Signed");
                            $('#show-approvals-btn').css({'background-color':'blue'});
                            $('#doc-signed-notice').text("This Agreement is Signed.");
                            $('#doc-signed-notice').css('background-color','blue');
                            $('#doc-signed-notice').css('color','white');
                           
                           } else {  
                            swal("Your Design Has Been Approved" ,'', "success");
                            $("button#approval-btn").text("Design Approved");
                            $('button#approval-btn').css('background-color','blue');
                        
                            $('#doc-signed-notice').hide();
                            $('#show-approvals-btn').hide();    
                            $('#request-change-btn').hide();
                            $('#send-ready-information').hide();
                            $('button#doc-approved').show();
                           }
                           SendDocApproved();
                        },
                    };
                

                    // We can also pass the url value separately from ajaxurl for front end AJAX implementations
                    $.post(ajax_object.ajax_url,  data,  function(response) {
                    //alert('Got this from the server: ' + response);
                        $('#isapproved').append(response);   
                    });
                 }
        });

        function SendDocApproved() {
            $the_doc_id = $('button#approval-btn').val();
            $the_approver_name = $('input#approver-name').val();
            $is_official = $('#approver-chbx').val();
            $date_signed = GetTodayDate();
            // send email to client before moving onto worldpay payment form
            var data = {
                action: 'doc_approved_send',
                post_id: $the_doc_id,
                the_approver_name: $the_approver_name,
                date_signed: $date_signed,
                is_official: $is_official,

                
                _ajax_nonce: $('#my_email_ajax_nonce').data('nonce'),
            };
            $.post(ajax_object.ajax_url,  data,  function(response) {
                console.log('Got this from the server: ' + response);
            });
            
            }

    $('button#doc-ready-btn').click(function() {
        $the_design_id = $('button#doc-ready-btn').val();
        $date_ready = GetTodayDate();
        $theposttype = $('#theposttype').val();
            if ($('#submitverify').val() == "SEND"){
            // send email to client before moving onto worldpay payment form
            var data = {
                action: 'doc_ready_send',
                post_id: $the_design_id,
                date_ready: $date_ready,
                success: function(){
                    if ($theposttype == 'agreements'){
                        swal("Agreement Review Sent",'', "success");
                    } else {
                    swal("Design Review Sent",'', "success")
                    }
                },
                _ajax_nonce: $('#my_email_ajax_nonce').data('nonce'),
            };
            $.post(ajax_object.ajax_url,  data,  function(response) {
                console.log('Got this from the server: ' + response);
            });
            } else {swal('Please verify that you want to submit for review');

                    };
    })


        $(function() {
            //----- OPEN
            $('[data-popup-open]').on('click', function(e)  {
            var targeted_popup_class = jQuery(this).attr('data-popup-open');
            $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
            e.preventDefault();
            });
            //----- CLOSE
            $('[data-popup-close]').on('click', function(e)  {
            var targeted_popup_class = jQuery(this).attr('data-popup-close');
            $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
            e.preventDefault();
            });
            });

        })//doc ready

});