
<?php
/*-------------------------------------------------------------
 Name:      rdas_move_comment_field, rdas_alter_comment_form_fields,
            rdas_add_comment_fields(

 Purpose:   Customizes the comments form and fields
-------------------------------------------------------------*/
    
    function rdas_move_comment_field( $fields ) {
        $comment_field = $fields['comment'];
        unset( $fields['comment'] );
        $fields['comment'] = $comment_field;
        return $fields;
    }
    add_filter( 'comment_form_fields', 'rdas_move_comment_field' );

    function rdas_alter_comment_form_fields($fields) {
        //unset($fields['author']);
        unset($fields['email']);
        unset($fields['url']);
        unset($fields['age']);
        return $fields;
    }
    add_filter('comment_form_default_fields', 'rdas_alter_comment_form_fields');
        
    function rdas_add_comment_fields($fields) {
        if( is_singular( 'rdas-designs' ) ) {
            $doc_id = get_the_ID();  
            $client_name = get_post_meta( $doc_id, 'company_name', true );
            
            $fields['author'] = '<input class="request-changes-fld" id="author" name="author" type="text" size="30" value="'. $client_name . '" /></p>';

               /* $fields['author'] = '<p class="comment-form-author request-changes-fld"><label for="author">' . __( 'Name | Company' ) . ' </label>' .
                '<input class="request-changes-fld" id="author" name="author" type="text" size="30" value="'. $client_name . '" /></p>';*/
        }
        return $fields;

        }
        add_filter('comment_form_default_fields','rdas_add_comment_fields');    
    
        


