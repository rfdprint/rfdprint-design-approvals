
<?php
/*
Template Name: Agreements Template
Template Post Type: designs, page
*/
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
get_header(); 


?>

<div class="full-width">
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
        <?php     
        $doc_id = get_the_ID();    
        $doc_approved = get_post_meta( $doc_id , 'doc_approved_chbx',true);
        if ($doc_approved  == true):   ?>   
            <p id="doc-signed-notice-yes">This Agreement is Signed (See Below).</p> 
            <?php else :?>
                <p id="doc-signed-notice">This agreement can be signed below.</p>
        <?php endif; ?>

        <?php while ( have_posts() ) : the_post(); 
        $theplanarray = get_post_meta(get_the_ID(), 'management_plan');
        $theplan = $theplanarray[0];

        $thecompanynamearray = get_post_meta(get_the_ID(), 'company_name'); 
        $thecompanyname = $thecompanynamearray[0];

        switch ($theplan) {
            case "essential":
            $doctitle = "Essentials Plan Agreement";
            $doc = file_get_contents(plugin_dir_path(__FILE__) . '/wwm-essentials.html');
            $html = str_replace(['%%DOCREFNUMBER%%', '%%COMPANYNAME%%','%%DOCTITLE%%'], [$doc_id, $thecompanyname, $doctitle]  , $doc);
            echo $html;         
                break;

            case 'premium':
            $doctitle = "Premium Plan Agreement";
            $doc = file_get_contents(plugin_dir_path(__FILE__) . '/wwm-premium.html');
            $html = str_replace(['%%DOCREFNUMBER%%', '%%COMPANYNAME%%','%%DOCTITLE%%'], [$doc_id, $thecompanyname, $doctitle]  , $doc);
            echo $html;  
                break;
            case 'business-boost':
            $doctitle = "Business Boost Agreement";
            $doc = file_get_contents(plugin_dir_path(__FILE__) . '/wwm-business-boost.html');
            $html = str_replace(['%%DOCREFNUMBER%%', '%%COMPANYNAME%%','%%DOCTITLE%%'], [$doc_id, $thecompanyname, $doctitle]  , $doc);
            echo $html;
            break;
            case 'social-media':
            $doctitle = "Social Media Agreement";
            $doc = file_get_contents(plugin_dir_path(__FILE__) . '/ssm-standard.html');
            $html = str_replace(['%%DOCREFNUMBER%%', '%%COMPANYNAME%%','%%DOCTITLE%%'], [$doc_id, $thecompanyname, $doctitle]  , $doc);
            echo $html;
            break;      
            case 'license':
            $doctitle = "License Agreement";
            $doc = file_get_contents(plugin_dir_path(__FILE__) . '/la-license.html');
            $html = str_replace(['%%DOCREFNUMBER%%', '%%COMPANYNAME%%','%%DOCTITLE%%'], [$doc_id, $thecompanyname, $doctitle]  , $doc);
            echo $html;
            break;    
            default:
                echo "No agreement selected";
        };
                ?>
                <?php if ($doc_approved  == true):   ?>
                    <button id="doc-approved" >Agreement Signed</button>
                    <?php else :?>
            
                        <a id="show-approvals-btn" class="btn" data-popup-open="popup-2" href="#">Sign This Agreement</a>
                        <div class="popup" data-popup="popup-2">
                        <div class="popup-inner">

                         <div id="approvals-form">
                                    Sign Name<br>
                                    <input id="approver-name" type="text" name="approvername" ></input> <br>
                                    <input id="doc-terms-chbx" type="checkbox" name="design-terms-chbx"></input>
                                    <?php echo '<label>By clicking this box I accept the terms and conditions of this agreement reference# ' .$doc_id. '.</label><br>';?> 
                                    <?php $thetype =  get_post_type()?>
                                    <input id="approver-chbx" type="checkbox" name="approverchbx">
                                    <?php echo '<input id="theposttype" type="hidden" name="theposttype" value="'. $thetype.'"</input>'?>
                                    <?php echo '<label>By clicking this box I affirm that the name I entered above represents my legal signature.</label><br>';?> 
                                    
                        <?php echo '<button id="approval-btn" type="submit" value='.$doc_id.'>Sign Agreement</button>'?><br>
                        </div>
                       
                        <p><a data-popup-close="popup-2" href="#">Close</a></p>
                        <a class="popup-close" data-popup-close="popup-2" href="#">x</a>
                        </div>
                        </div>  
                <?php endif  ?>       
                <div id="doc-left-panel">
                <div class="panel-title-bar">
                <h3 class="panel-title">Additional Services & Information</h3>
                </div>
                <div id="agreement-information">
                <?php                       
                        $doc_notes = get_post_meta( $doc_id , 'doc_notes', true);
                        echo '<p class="panel-info"> '. $doc_notes . '</span></p>';
                 ?>
                </div>  
                </div> <!-- #doc-left-panel-->
                <div id="doc-right-panel">
                <div class="panel-title-bar">
                <h3 class="panel-title">Agreement Status Information</h3>
                </div>
                    <div id="project-information">
                    <?php 
                        $project_id = get_post_meta( $doc_id , 'rdas_design_projects',true);
                        $project_name = get_the_title( $project_id );
                        echo '<p class="panel-info-label">Document Name: <span class="panel-info"> '. $project_name. '</span></p>';
                        
                        $client_name = get_post_meta( $doc_id , 'company_name',true);
                        echo '<p class="panel-info-label">Client Name: <span class="panel-info"> '. $client_name . '</span></p>';
                        ?>
                    </div>  
                    
                    <?php  
                  
                        if ($doc_approved  == true):   ?>
                    <div id="approval-information">
                        <?php 
                            $approver_name = get_post_meta( $doc_id , 'approver_name',true);
                            
                            echo '<p class="panel-info-label">Signer Name: <span class="panel-info"> '. $approver_name . '</span></p>'  ;

                           $date_approved = get_post_meta( $doc_id , 'date_doc_approved',true);
                            echo '<p class="panel-info-label">Signed On: <span class="panel-info"> '.$date_approved . '</span></p>';
                            ?>
                    </div>   
                <?php endif;
                    if(current_user_can('administrator') && $doc_approved  == false ){?>
                        <div id="send-ready-information">
                             <form name="submit-verify-form" action="" method="post">
                                    <input id="submitverify" type="text" name="submitverify" placeholder="Confirm Review" ></input>
                            </form>
                            <?php  echo '<button id="doc-ready-btn"  value='.$doc_id.'>Submit for Review</button>';
                            $currentdoclog = get_post_meta( $doc_id, 'rdas_document_log', true );              
                             echo '<div id="doc-detail-log">'. $currentdoclog .'</div>';       
                    }?>
                         </div>
                </div> <!-- #doc-right-panel-->    

    <?php    endwhile; // end of the loop. ?>
        
        </main><!-- #main -->
    </div><!-- #primary -->
</div>
<?php //get_sidebar(); ?>
<span id="my_email_ajax_nonce" data-nonce="<?php echo wp_create_nonce( 'my_email_ajax_nonce' ); ?>"></span>

<?php 
get_footer(); ?>

