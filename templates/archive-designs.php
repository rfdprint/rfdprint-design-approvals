
<?php
/*
Template Name: Design Archive
Template Post Type: designs
*/
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
get_header(); ?>

<div class="full-width">
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

            
        <?php while ( have_posts() ) : the_post(); ?>
                
                <div id="design-content">
                <?php the_content(); ?>
                </div>
                <?php $DesignID = get_the_ID();
                 $design_approved = get_post_meta( $DesignID , 'design_approved_chbx',true);
                ?>
                <?php if ($design_approved  == true):   ?>
                    <button id="design-approved" >Design Approved</button>
                    <?php else :?>
            
                        <a id="show-approvals-btn" class="btn" data-popup-open="popup-2" href="#">Approve Design</a>
                        <div class="popup" data-popup="popup-2">
                        <div class="popup-inner">

                         <div id="approvals-form">
                                    Enter Approver Name<br>
                                    <input id="approver-name" type="text" name="approvername" ></input> <br>
                                    <input id="approver-chbx" type="checkbox" name="approverchbx"></input>
                                    The name entered above represent my legal signature<br>
                                    
                        <?php echo '<button id="approval-btn" type="submit" value='.$DesignID.'>Approve Design</button>'?><br>
                        </div>
                       
                        <p><a data-popup-close="popup-2" href="#">Close</a></p>
                        <a class="popup-close" data-popup-close="popup-2" href="#">x</a>
                        </div>
                        </div>  
                <?php endif  ?>       
                       
            <?php
            
                 if ($design_approved  == false):   ?>
                    <a id="request-change-btn" class="btn" data-popup-open="popup-3" href="#">Request Changes</a>
                    <div class="popup" data-popup="popup-3">
                    <div class="popup-inner">
                    <div id="request-changes-div">
                             
                        <?php  
                            function wpb_alter_comment_form_fields($fields) {
                                //unset($fields['author']);
                                unset($fields['email']);
                                unset($fields['url']);
                                unset($fields['age']);
                                return $fields;
                            }
                            add_filter('comment_form_default_fields', 'wpb_alter_comment_form_fields');
                            
                            function add_comment_fields($fields) {
        
                                if( is_singular( 'designs' ) ) {
                                    $post_id = get_the_ID();
                                    $client_name = get_post_meta( $post_id, 'company_name', true );
                                    
                                    $fields['author'] = '<p class="comment-form-author request-changes-fld"><label for="author">' . __( 'Company | Name' ) . ' </label>' .
                                        '<input class="request-changes-fld" id="author" name="author" type="text" size="30" value="'. $client_name . '" /></p>';
                                }
                                return $fields;
                        
                                }
                                add_filter('comment_form_default_fields','add_comment_fields');    
                        
                            $comments_args = array(
                                // change the title of send button 
                                'label_submit'=>'Send Request',
                                // change the title of the reply section
                                'title_reply'=>'Request Design Changes',
                                // remove "Text or HTML to be displayed after the set of comment fields"
                                'comment_notes_before' => '',
                                // remove "Text or HTML to be displayed after the set of comment fields"
                                'comment_notes_after' => '',
                                // redefine your own textarea (the comment body)
                                'comment_field' => '<p class="comment-form-comment"><br /><textarea id="comment" name="comment" placeholder="Enter any design change request here." aria-required="true"></textarea></p>',
                                // remove loggin name
                                'logged_in_as' => '',
                                
                                'id_submit' => 'send-request-btn',
                                'id_form' => 'request-change-form',
                            
                            );
                            
                            comment_form($comments_args); 
                            
                            ?> 
                                                                        
                            
                    <p><a data-popup-close="popup-3" href="#">Close</a></p>
                    <a class="popup-close" data-popup-close="popup-3" href="#">x</a>
                    </div>   
                    </div>
                    </div>  
                 <?php endif  ?>               
            <?php 
                //$design_projectID = get_field('rdas_design_projects');
                $design_projectID = get_post_meta( $post->ID, 'rdas_design_projects', true);

                // the query
                $args = array(
                    'post_type'=>'designs', // Your post type name
                    'meta_key' => 'rdas_design_projects',
                    'meta_value' => $design_projectID
                );

            ?>

                <?php
                
                // the loop************************************************ ?>
                <div id="design-left-panel">
                <div class="panel-title-bar">
                <h3 class="panel-title">Design Information</h3>
                </div>
                <div id="design-information">
                <?php 
                        $project_id = get_post_meta( $DesignID , 'rdas_design_projects',true);
                        $design_version = get_post_meta( $DesignID , 'version_number',true);
                        echo '<p class="panel-info-label">Version: <span class="panel-info"> '. $design_version . '</span></p>';
                        
                        $design_specs = get_post_meta( $DesignID , 'design_specs',true);
                        echo '<p class="panel-info-label">Design Specs: <span class="panel-info"> '. $design_specs . '</span></p>';

                        $design_notes = get_post_meta( $DesignID , 'design_notes',true);
                        echo '<p class="panel-info-label">Design Notes: <span class="panel-info"> '. $design_notes . '</span></p>';
                        ?>
                 </div>  

                <a id="show-versions-btn" class="btn button" data-popup-open="popup-1" href="#">Show All Versions</a>
                <div class="popup" data-popup="popup-1">
                    <div class="popup-inner">
                    <h4 id="all-designs-title">Current Design Versions</h4>
                    <div id="previous_versions-div">
                    <?php
                        $loop = new WP_Query( $args );
                        if ( $loop->have_posts() ) :
                            while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                       <?php $design_version_id = get_the_id();?>
                                        <a class="li-previous-design button" href="<?php get_permalink(); ?>"><?php the_field('version_number', $design_version_id); ?></a>    
    
                            <?php endwhile; 
                            
                        endif;     ?> 
                    </div> 

                        <p><a data-popup-close="popup-1" href="#">Close</a></p>
                        <a class="popup-close" data-popup-close="popup-1" href="#">x</a>
                    </div>
                </div>        

                </div> <!-- #design-left-panel-->
                <div id="design-right-panel">
                <div class="panel-title-bar">
                <h3 class="panel-title">Project Information</h3>
                </div>
                    <div id="project-information">
                    <?php 
                        $project_id = get_post_meta( $DesignID , 'rdas_design_projects',true);
                        $project_name = get_the_title( $project_id );
                        echo '<p class="panel-info-label">Project: <span class="panel-info"> '. $project_name. '</span></p>';
                        
                        $client_name = get_post_meta( $DesignID , 'company_name',true);
                        echo '<p class="panel-info-label">Client: <span class="panel-info"> '. $client_name . '</span></p>';
                        ?>
                    </div>  
                    
                    <?php  
                        $design_approved = get_post_meta( $DesignID , 'design_approved_chbx',true);
                        if ($design_approved  == true):   ?>
                    <div id="approval-information">
                        <?php 
                            $approver_name = get_post_meta( $DesignID , 'approver_name',true);
                            
                            echo '<p class="panel-info-label">Approver Name: <span class="panel-info"> '. $approver_name . '</span></p>'  ;

                            $date_design_approved = get_post_meta( $DesignID , 'date_design_approved',true);
                            echo '<p class="panel-info-label">Approved Date: <span class="panel-info"> '. $date_design_approved . '</span></p>';
                            ?>
                    </div>   
                <?php endif;
                    if(current_user_can('administrator') && $design_approved  == false ){?>
                        <div id="send-ready-information">
                             <form name="submit-verify-form" action="" method="post">
                                    <input id="submitverify" type="text" name="submitverify" placeholder="Confirm Review" ></input>
                            </form>
                            <?php  echo '<button id="design-ready-btn"  value='.$DesignID.'>Submit for Review</button>';?>
                            <?php    }?>
                         </div>
                </div> <!-- #design-right-panel-->    

    <?php    endwhile; // end of the loop. ?>
         <?php
                 
            // If comments are open or we have at least one comment, load up the comment template
            if ( comments_open() || '0' != get_comments_number() ) :
                comments_template();
            endif;
        ?> 


        </main><!-- #main -->
    </div><!-- #primary -->
</div>
<?php //get_sidebar(); ?>
<span id="my_email_ajax_nonce" data-nonce="<?php echo wp_create_nonce( 'my_email_ajax_nonce' ); ?>"></span>
<?php get_footer(); ?>

