
<?php
/*
Template Name: Desigs Template
Template Post Type: designs
*/
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
get_header(); 
?>


<div class="full-width">
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">  
        <?php while ( have_posts() ) : the_post(); ?>
                
                <div id="design-content">
                <?php
                    $doc_id = get_the_ID(); 
                    $doc_approved = get_post_meta( $doc_id , 'doc_approved_chbx',true);
                    if ($doc_approved  == true):   ?>   
                        <p id="doc-signed-notice-yes">This design has been approved (See Below).</p> 
                        <?php else :?>
                            <p id="doc-signed-notice">This design can be approved below.</p>
                    <?php endif; ?>
                <?php the_content();?> 
                <div class="watermark-notice">
                <p>The RFDPrint watermark will be removed from the final product.</p>
                </div>
                <button id="doc-approved" style="display:none;">Design Approved</button>
               </div>
               <?php    endwhile; // end of the loop.?>

                
                <?php if ($doc_approved  == true):   ?>
                    <button id="doc-approved" >Design Approved</button>
                    <?php else :?>
            
                         <a id="show-approvals-btn" class="btn" data-popup-open="popup-2" href="#">Approve Design</a>
                        <div class="popup" data-popup="popup-2">
                        <div class="popup-inner">

                         <div id="approvals-form">
                                 <!--  <label id="approver-name-label"> Enter Approver Name</label><br>
                                    <input id="approver-name" type="text" name="approvername" ></input> <br>-->
                                    <div id="approval-checkboxes"> 
                                        
                                       <input id="approver-chbx" type="checkbox" name="approverchbx"></input>
                                        <label>I accept the terms & conditions</label><br> <!-- and affirm that the name entered above represents my legal signature.-->
                                    
                                        <?php $thetype =  get_post_type()?>
                                        <?php echo '<input id="theposttype" type="hidden" name="theposttype" value="'. $thetype.'"</input>'?>
                                    </div>
                                    
                        <?php echo '<button id="approval-btn" type="submit" value='.$doc_id.'>Approve Design</button>'?><br>
                        <p class="required-field-notice">All fields are required.</p>
                     </div>
                        <p><a data-popup-close="popup-2" href="#">Close</a></p>
                        <a class="popup-close" data-popup-close="popup-2" href="#">x</a>
                       </div>
                        </div>  
                <?php endif  ?>       
                       
            
                <?php if ($doc_approved  == false):   ?>
                    <a id="request-change-btn" class="btn" data-popup-open="popup-3" href="#">Request Changes</a>
                    <div class="popup" data-popup="popup-3">
                    <div class="popup-inner">
                    <div id="request-changes-div">
                             
                        <?php 
                        /*Change the comment notifaction subject*/
                            $comments_args = array(
                                // change the title of send button 
                                'label_submit'=>'Send Request',
                                // change the title of the reply section
                                'title_reply'=>'Request Design Changes',
                                // remove "Text or HTML to be displayed after the set of comment fields"
                                'comment_notes_before' => '',
                                // remove "Text or HTML to be displayed after the set of comment fields"
                                'comment_notes_after' => '',
                                // redefine your own textarea (the comment body)
                                'comment_field' => '<p class="comment-form-comment"><br /><textarea id="comment" name="comment" placeholder="Enter any design change request here." aria-required="true"></textarea></p>',
                                // remove loggin name
                                'logged_in_as' => '',
                                
                                'id_submit' => 'send-request-btn',
                                'id_form' => 'request-change-form',
                            
                            );
                            comment_form($comments_args); 
                            
                            ?> 
                                                                        
                            
                    <p><a data-popup-close="popup-3" href="#">Close</a></p>
                    <a class="popup-close" data-popup-close="popup-3" href="#">x</a>
                    </div>   
                    </div>
                    </div>  
                 <?php endif  ?> 
            <a id="showterms">Show Terms & Conditions</a>
            <p id="thedesignterms" style="display:none;"><strong>TERMS AND CONDITIONS </strong>Designs cannot be changed after approval. You, the business owner or representative, accept any and all responsibility for elements, offers, and content of the approved design. You the business owner or representative, accept full responsibility and certify that you own or have license rights to any materials or content submitted to RFDPrint by you or your representative that are included on this design and release RFDPrint from any litigation, disputes, loss of business or finances as result of violations of copyright, trademark or privacy laws. RFDPrint has the right to use this approved design on printed or online content as samples, templates and/or promotions of RFDPrint.</p>
                                

            <?php 
                //$design_projectID = get_field('rdas_design_projects');
                $design_projectID = get_post_meta( $post->ID, 'rdas_design_projects', true);

                // the query
                $args = array(
                    'post_type'=>'designs', // Your post type name
                    'meta_key' => 'rdas_design_projects',
                    'meta_value' => $design_projectID,
                    //'order'   => 'ASC',
                );

            ?>

                <?php
                // the loop************************************************ ?>
                <div id="doc-left-panel">
                <div class="panel-title-bar">
                <h3 class="panel-title">Design Information</h3>
                </div>
                <div id="design-information">
                <?php 
                        $project_id = get_post_meta( $doc_id , 'rdas_design_projects',true);
                        $design_version = get_post_meta( $doc_id , 'version_number',true);
                        echo '<p class="panel-info-label">Version: <span class="panel-info"> '. $design_version . '</span></p>';
                        
                        $design_specs = get_post_meta( $doc_id , 'design_specs',true);
                        echo '<p class="panel-info-label">Design Specs: <span class="panel-info"> '. $design_specs . '</span></p>';

                        $doc_notes = get_post_meta( $doc_id , 'doc_notes',true);
                        echo '<p class="panel-info-label">Design Notes: <span class="panel-info"> '. $doc_notes . '</span></p>';
                        ?>
                 </div>  
                <a id="show-versions-btn" class="btn button" data-popup-open="popup-1" href="#">Show All Versions</a>
                <div class="popup" data-popup="popup-1">
                    <div class="popup-inner">
                    <h4 id="all-designs-title">Other Design Versions</h4>
                    <div id="previous_versions-div">
                    <?php
                        $thecurrentversion = get_the_ID(); 
                        $loop = new WP_Query( $args );
                       
                        if ( $loop->have_posts() ) :
                            if (isset($project_id)){
                                $versioncount = 0;
                                while ( $loop->have_posts() ) : $loop->the_post(); 
                                        $design_version_id = get_the_id();
                                        $versionnumber = get_post_meta( $design_version_id, 'version_number', true );
                                        if ($design_version_id != $thecurrentversion ){
                                            echo '<a class="li-previous-design button" href="'.get_permalink() .'">'.$versionnumber.'</a>';    
                                            $versioncount++;
                                        }  else { 
                                            echo '<a class="li-current-design button" href="'.get_permalink() .'">'.$versionnumber.' (Viewing Now)</a>';
                                        }
                                endwhile; 
                                wp_reset_query();
                            }
                            if ($versioncount <1) {
                            echo '<p style="text-align:center;">There are no other versions of this design.</p>';
                            }
                        endif;     ?> 
                    </div> 

                        <p><a data-popup-close="popup-1" href="#">Close</a></p>
                        <a class="popup-close" data-popup-close="popup-1" href="#">x</a>
                    </div>
                </div>        

                </div> <!-- #doc-left-panel-->
                <div id="doc-right-panel">
                <div class="panel-title-bar">
                <h3 class="panel-title">Project Information</h3>
                </div>
                    <div id="project-information">
                    <?php 
                        $project_id = get_post_meta( $doc_id , 'rdas_design_projects',true);
                        $project_name = get_the_title( $project_id );
                        $doc_name = get_the_title();
                        if (!empty($project_name)){
                        echo '<p class="panel-info-label">Project: <span class="panel-info"> '. $project_name. '</span></p>';
                        } else {
                            echo '<p class="panel-info-label">Project: <span class="panel-info"> '. $doc_name . '</span></p>';
                        }

                        $client_name = get_post_meta( $doc_id , 'company_name',true);
                        echo '<p class="panel-info-label">Client: <span class="panel-info"> '. $client_name . '</span></p>';
                        ?>
                    </div>  
                    
                    <?php  
                        $design_approved = get_post_meta( $doc_id , 'doc_approved_chbx',true);
                        if ($design_approved  == true):   ?>
                    <div id="approval-information">
                        <?php 
                           // $approver_name = get_post_meta( $doc_id , 'approver_name',true);
                            
                           // echo '<p class="panel-info-label">Approver Name: <span class="panel-info"> '. $approver_name . '</span></p>'  ;

                            $date_doc_approved = get_post_meta( $doc_id , 'date_doc_approved',true);
                            echo '<p class="panel-info-label">Approved Date: <span class="panel-info"> '. $date_doc_approved . '</span></p>';
                            ?>
                    </div>   
                <?php endif;
                    if(current_user_can('administrator') && $design_approved  == false ){?>
                        <div id="send-ready-information">
                             <form name="submit-verify-form" action="" method="post">
                                    <input id="submitverify" type="text" name="submitverify" placeholder="Confirm Review" ></input>
                            </form>
                            <?php  echo '<button id="doc-ready-btn"  value='.$doc_id.'>Submit for Review</button>';
                            $currentdoclog = get_post_meta( $doc_id, 'rdas_document_log', true );              
                             echo '<div id="doc-detail-log">'. $currentdoclog .'</div>';       
                    }?>
                         </div>
                         <?php 
                        ?>
                </div> <!-- #doc-right-panel-->  
                <?php
                 
                 // If comments are open or we have at least one comment, load up the comment template
                
                 if ( comments_open($doc_id) ):// || '0' != get_comments_number($doc_id) ) :
                     comments_template($doc_id);
                 endif;
             ?>   

        </main><!-- #main -->
    </div><!-- #primary -->
</div>
<?php //get_sidebar(); ?>
<span id="my_email_ajax_nonce" data-nonce="<?php echo wp_create_nonce( 'my_email_ajax_nonce' ); ?>"></span>
<?php get_footer(); ?>

